package com.mowdownDevelopments.smitebible;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by david on 6/1/13.
 */
public class APIHelper {
    private static APIHelper instance = null;
    private Context cxt = null;
    private static String sessionID = null;
    private final String uriBase = "http://api.smitegame.com/smiteapi.avc/";

    private APIHelper(Context c) {
        cxt = c;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        cxt = null;
    }

    static APIHelper getInstance(Context c) {
        if (instance == null) {
            instance = new APIHelper(c.getApplicationContext());
        }
        return instance;
    }

    protected String generateSignature(String method) throws NoSuchAlgorithmException {
        StringBuilder builder = new StringBuilder().append(cxt.getResources().getInteger(R.integer.DevID))
                .append(method).append(cxt.getString(R.string.APIKey)).append(generateUtcTimestamp());
        MessageDigest digester = MessageDigest.getInstance("MD5");
        byte[] bytes = builder.toString().getBytes();
        byte[] digest = digester.digest(bytes);
        return bytesToHex(digest);
    }

    private long generateUtcTimestamp() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        return Long.parseLong(new StringBuilder().append(year).append(month).append(day).append(hour).append(minute)
                .append(second).toString());
    }

    private String bytesToHex(byte[] bytes) {
	    final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for (int j = 0; j < bytes.length; j++) {
	        v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}

	public static String getSessionID() {
        return sessionID;
    }

    public String createSessionIfNotOpen() throws NoSuchAlgorithmException {
        if (sessionID == null) {
            StringBuilder builder = new StringBuilder(uriBase).append("createsessionJson/")
                    .append(cxt.getResources().getInteger(R.integer.DevID)).append('/').append(generateSignature("createsession"))
                    .append('/').append(generateUtcTimestamp());
            //Ion library used to handle networking. This instance loads from a JSONObject from given URL to a given context and performs the callback when complete.
            Ion.with(cxt, builder.toString()).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
                @Override
                public void onCompleted(Exception e, JsonObject result) {
                    if (e != null) {
                        Log.e(this.getClass().getSimpleName(), "There was a networking problem!", e);
                        Toast.makeText(cxt, R.string.networking_error, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        sessionID = result.get("session_id").getAsString();
                    } catch (ClassCastException e1) {
                        Log.e(this.getClass().getSimpleName(), "Error parsing the SessionID", e1);
                        sessionID = null;
                    }
                }
            });
        }
        return sessionID;

    }

    public String getItemsUri() throws NoSuchAlgorithmException {
	    if (ping()) {
	        StringBuilder builder = new StringBuilder(uriBase).append("getitemsJson/")
	                .append(cxt.getResources().getInteger(R.integer.DevID)).append('/').append(generateSignature("getitems"))
	                .append('/').append(createSessionIfNotOpen()).append('/').append(generateUtcTimestamp()).append('/');
	        if (Locale.getDefault() == Locale.FRENCH || Locale.getDefault() == Locale.CANADA_FRENCH || Locale.getDefault() == Locale.FRANCE) {
	            builder.append(3);
	        } else {
	            builder.append(1);
	        }
	        return builder.toString();
	    } else {
	        return null;
	    }
	}

	public String getPlayerUri(String playerName) throws NoSuchAlgorithmException {
	    if (ping()) {
	        StringBuilder builder = new StringBuilder(uriBase).append("getplayerJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getplayer")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(playerName);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}

	public String getMatchDetailsUri(long mapID) throws NoSuchAlgorithmException{
	    if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("getmatchdetailsJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getmatchdetails")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(mapID);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}

	public String getMatchHistoryUri(String playerName) throws NoSuchAlgorithmException{
	    if (ping()) {
	        StringBuilder builder = new StringBuilder(uriBase).append("getmatchhistoryJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getmatchhistory")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(playerName);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}

	public String getQueueStatsUri(String playerName, int queueID) throws NoSuchAlgorithmException {
	    if (ping()) {
	        StringBuilder builder = new StringBuilder(uriBase).append("getqueuestatsJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getqueuestats")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(playerName).append('/').append(queueID);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}

	public String getTopRankedPlayersUri() throws NoSuchAlgorithmException{
	    if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("gettoprankedJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("gettopranked")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp());
	        return builder.toString();
	    } else {
	        return null;
	    }
	}

	public String getDataUsedUri() throws NoSuchAlgorithmException{
	    if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("getdatausedJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getdataused")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp());
	        return builder.toString();
	    } else {
	        return null;
	    }
	}

	public String getGodsUri() throws NoSuchAlgorithmException{
	    if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("getgodsJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getgods")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/');
	        if (Locale.getDefault() == Locale.FRENCH || Locale.getDefault() == Locale.CANADA_FRENCH || Locale.getDefault() == Locale.FRANCE) {
	            builder.append(3);
	        } else {
	            builder.append(1);
	        }
	        return builder.toString();
	    } else {
	        return null;
	    }
	}
	
	public String getDemoDetailsUri(long matchID) throws NotFoundException, NoSuchAlgorithmException{
		if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("getdemodetailsJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getdemodetails")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(matchID);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}
	
	public String searchTeamsUri(String searchTerm) throws NotFoundException, NoSuchAlgorithmException{
		if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("searchteamsJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("searchteams")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(searchTerm);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}
	
	public String getTeamMatchHistoryUri(long teamId) throws NotFoundException, NoSuchAlgorithmException{
		if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("getteammatchhistoryJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getteammatchhistory")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(teamId);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}
	
	public String getTeamPlayersUri(long teamID) throws NotFoundException, NoSuchAlgorithmException{
		if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("getteamplayersJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getteamplayers")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(teamID);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}
	
	public String getTeamLeaderboardUri() throws NotFoundException, NoSuchAlgorithmException{
		if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("getteamleaderboardsJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getteamleaderboards")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp());
	        return builder.toString();
	    } else {
	        return null;
	    }
	}
	
	public String getTeamDetails(long teamId) throws NotFoundException, NoSuchAlgorithmException{
		if (ping()){
	        StringBuilder builder = new StringBuilder(uriBase).append("getteamdetailsJson/").append(cxt.getResources().getInteger(R.integer.DevID))
	                .append('/').append(generateSignature("getteamdetails")).append('/').append(createSessionIfNotOpen()).append('/')
	                .append(generateUtcTimestamp()).append('/').append(teamId);
	        return builder.toString();
	    } else {
	        return null;
	    }
	}

	// Returns if ping was successful.
    public boolean ping() {
        final boolean[] pingSuccess = new boolean[1];
        Ion.with(cxt, new StringBuilder(uriBase).append("pingJson").toString()).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                if (e != null) {
                    Log.e(this.getClass().getSimpleName(), "There was a networking problem!", e);
                    Toast.makeText(cxt, R.string.networking_error, Toast.LENGTH_SHORT).show();
                    pingSuccess[0] = false;
                    return;
                }
                if (result == null) {
                    Log.e(this.getClass().getSimpleName(), "No response.");
                    Toast.makeText(cxt, R.string.no_response_from_ping, Toast.LENGTH_SHORT).show();
                    pingSuccess[0] = false;
                    return;
                } else {
                    Log.i(this.getClass().getSimpleName(), result.toString());
                }
                pingSuccess[0] = true;
            }
        });
        return pingSuccess[0];
    }
}
