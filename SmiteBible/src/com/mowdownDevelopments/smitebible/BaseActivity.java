package com.mowdownDevelopments.smitebible;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import com.commonsware.cwac.merge.MergeAdapter;

public class BaseActivity extends Activity implements ListView.OnItemClickListener, NagFragment.OnFragmentInteractionListener {

	private DrawerLayout drawerLayout;
	protected static final String favPrefFile = "playerNames", favKey = "favPlayers", userKey = "defaultName";
	private ListView drawerList;
	private ActionBarDrawerToggle drawerToggle;
	private ArrayAdapter<String> baseDrawerAdapter, playerDrawerAdapter;
	private MergeAdapter comboAdapter;
	private List<String> favPlayersList;
	private static final String FILENAME = "ingamenames";

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base);
		favPlayersList = new LinkedList<String>();
		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		drawerLayout = (DrawerLayout) findViewById(R.id.Drawer_Layout);
		drawerList = (ListView) findViewById(R.id.Drawer_List);
		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.open_pane, R.string.closed_pane);
		drawerLayout.setDrawerListener(drawerToggle);
		ListAdapter adapter= setUpDrawerListAdapter(); 
		drawerList.setAdapter(adapter);
		drawerList.setOnItemClickListener(this);
		//To Be Changed once past testing phase:
		switchMainView(new JSONTestFragment(), null);
	}

	protected ListAdapter setUpDrawerListAdapter(){
		boolean addedToList = false;
		String firstPlayerName = PreferenceManager.getDefaultSharedPreferences(this).getString(userKey, null);
		Set<String> favPlayers = getSharedPreferences(favPrefFile, 0).getStringSet(favKey, null);
		List<String> drawerListItems = Arrays.asList(getResources().getStringArray(R.array.drawer_options));
		baseDrawerAdapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, drawerListItems);
		if (!firstPlayerName.isEmpty() || firstPlayerName != null){
			favPlayersList.add(firstPlayerName);
			addedToList = true;
		}
		if (!favPlayers.isEmpty()){
			if (!firstPlayerName.isEmpty() || firstPlayerName != null){
				favPlayers.remove(firstPlayerName);
			}
			favPlayersList.addAll(favPlayers);
			addedToList = true;
		}
		comboAdapter = new MergeAdapter();
		comboAdapter.addAdapter(baseDrawerAdapter);
		if (addedToList){
			playerDrawerAdapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, favPlayersList);
			TextView header = new TextView(this);
			header.setTypeface(Typeface.DEFAULT_BOLD);
			header.setPadding(header.getPaddingLeft(), 42, header.getPaddingRight(), header.getPaddingBottom());
			header.setText(R.string.favHeader);
			comboAdapter.addView(header);
			comboAdapter.addAdapter(playerDrawerAdapter);
		} else {
			playerDrawerAdapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item);
			comboAdapter.addAdapter(playerDrawerAdapter);
		}
		return comboAdapter;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.base, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)){
			return true;
		}
		switch (item.getItemId()){
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
		int playerPositionSubtractor = 7;
		switch (position){
		case 0:
			//Gods
			break;
		case 1:
			//Items
			break;
		case 2:
			//Player search
			break;
		case 3:
			//top players
			break;
		case 4:
			//team search
			break;
		case 5:
			//team leaderboard
			break;
		case 6:
			if (view.getPaddingTop()==42) {
				//Do Nothing, assuming header view was properly instantiated.
				break;
			}
			//Else, continue to default and show the appropriate player's stat page.
			--playerPositionSubtractor;
		default:
			String playerName = favPlayersList.get(position-playerPositionSubtractor);
		}
	}

	@Override
	public void onUserInGameNameEntered(String name) {
		favPlayersList.remove(0);
		if (favPlayersList != null && favPlayersList.contains(name)){
			favPlayersList.remove(name);
		}
		favPlayersList.add(0, name);
		playerDrawerAdapter.notifyDataSetChanged();
	}
	
	public void refreshPlayerAdapter(){
		Set<String> favPlayers = getSharedPreferences(favPrefFile, 0).getStringSet(favKey, null);
		String userIGN = PreferenceManager.getDefaultSharedPreferences(this).getString(userKey, null);
		favPlayersList.clear();
		if (userIGN != null){
			favPlayers.remove(userIGN);
			favPlayersList.add(userIGN);
		}
		favPlayersList.addAll(favPlayers);
		playerDrawerAdapter.notifyDataSetChanged();
	}

	private void addNameToNameFile(String name) throws IOException{
		LinkedList<String> listFromFile = getNamesFromFile();
		if (listFromFile != null) {
			FileOutputStream fos = openFileOutput(FILENAME, 0);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			listFromFile.add(name);
			oos.writeObject(listFromFile);
			oos.close();
			bos.close();
			fos.close();
		} else {
			throw new IOException("Write to file failed.");
		}
	}

	private void addNameToPreferenceSet (String name){
		SharedPreferences prefs = getSharedPreferences(favPrefFile, 0);
		LinkedHashSet<String> setOfNames = new LinkedHashSet<String>(prefs.getStringSet("favPlayers", new LinkedHashSet<String>(200)));
		if (setOfNames.add(name)) {
			prefs.edit().putStringSet("favPlayers", setOfNames).commit();
		}
	}

	@SuppressWarnings("unchecked")
	private LinkedList<String> getNamesFromFile(){
		LinkedList<String> listFromFile;
		try {
			FileInputStream fis = openFileInput(FILENAME);
			BufferedInputStream bis = new BufferedInputStream(fis);
			ObjectInputStream ois = new ObjectInputStream(bis);
			listFromFile = (LinkedList<String>) ois.readObject();
			ois.close();
			bis.close();
			fis.close();
		} catch (FileNotFoundException e) {
			Log.w("FileNotFound", e);
			listFromFile = new LinkedList<String>();
		} catch (StreamCorruptedException e) {
			Log.e("BaseActivity", "Could not read file. Stream was corrupted.", e);
			return null;
		} catch (IOException e) {
			Log.e("BaseActivity", "Could not read file.", e);
			return null;
		} catch (ClassNotFoundException e) {
			Log.e("BaseActivity", "How did you get this error?", e);
			return null;
		}
		return listFromFile;
	}
	
	private int switchMainView(Fragment f, String name){
		return getFragmentManager().beginTransaction().replace(R.id.Content_View, f).addToBackStack(name).commit();
	}
}
