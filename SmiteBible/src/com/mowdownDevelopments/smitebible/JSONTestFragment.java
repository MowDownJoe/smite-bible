package com.mowdownDevelopments.smitebible;

import java.security.NoSuchAlgorithmException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Use the
 * {@link JSONTestFragment#newInstance} factory method to create an instance of
 * this fragment.
 * 
 */
public class JSONTestFragment extends Fragment implements View.OnClickListener {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";

	// TODO: Rename and change types of parameters
	private String mParam1;
	private String mParam2;
	private Button playerButton, godButton, topPlayerButton, itemButton,
			dataButton, queueButton, historyButton, matchButton,
			teamsearchbutton, demobutton;

	/**
	 * Use this factory method to create a new instance of this fragment using
	 * the provided parameters.
	 * 
	 * @param param1
	 *            Parameter 1.
	 * @param param2
	 *            Parameter 2.
	 * @return A new instance of fragment JSONTestFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static JSONTestFragment newInstance(String param1, String param2) {
		JSONTestFragment fragment = new JSONTestFragment();
		Bundle args = new Bundle();
		args.putString(ARG_PARAM1, param1);
		args.putString(ARG_PARAM2, param2);
		fragment.setArguments(args);
		return fragment;
	}

	public JSONTestFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			mParam1 = getArguments().getString(ARG_PARAM1);
			mParam2 = getArguments().getString(ARG_PARAM2);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_jsontest, container, false);
	}

	@Override
	public void onViewCreated(View v, Bundle savedInstanceState) {
		super.onViewCreated(v, savedInstanceState);
		godButton = (Button) v.findViewById(R.id.GodTestbutton);
		godButton.setOnClickListener(this);
		playerButton = (Button) v.findViewById(R.id.playerTestButton);
		playerButton.setOnClickListener(this);
		itemButton = (Button) v.findViewById(R.id.itemTestButton);
		itemButton.setOnClickListener(this);
		topPlayerButton = (Button) v.findViewById(R.id.topTestButton);
		topPlayerButton.setOnClickListener(this);
		dataButton = (Button) v.findViewById(R.id.datausedbutton);
		dataButton.setOnClickListener(this);
		historyButton = (Button) v.findViewById(R.id.matchhistorybutton);
		historyButton.setOnClickListener(this);
		matchButton = (Button) v.findViewById(R.id.matchdetailsbutton);
		matchButton.setOnClickListener(this);
		queueButton = (Button) v.findViewById(R.id.queuestatsbutton);
		queueButton.setOnClickListener(this);
		demobutton = (Button) v.findViewById(R.id.demotestbutton);
		demobutton.setOnClickListener(this);
		teamsearchbutton = (Button) v.findViewById(R.id.teamsearchtestbutton);
		teamsearchbutton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		APIHelper helper = APIHelper.getInstance(getActivity());
		switch (v.getId()) {
		case R.id.GodTestbutton:
			try {
				Ion.with(getActivity(), helper.getGodsUri()).asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.playerTestButton:
			try {
				Ion.with(getActivity(), helper.getPlayerUri("mowdownjoe"))
						.asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {

							@Override
							public void onCompleted(Exception e, JsonObject json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e) {
				Log.e("TestFragment", "Test failed", e);
			}
			break;
		case R.id.itemTestButton:
			try {
				Ion.with(getActivity(), helper.getItemsUri()).asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.topTestButton:
			try {
				Ion.with(getActivity(), helper.getTopRankedPlayersUri())
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.datausedbutton:
			try {
				Ion.with(getActivity(), helper.getDataUsedUri()).asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {

							@Override
							public void onCompleted(Exception e, JsonObject json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e) {
				Log.e("TestFragment", "Test failed", e);
			}
			break;
		case R.id.matchhistorybutton:
			try {
				Ion.with(getActivity(), helper.getMatchHistoryUri("mowdownjoe"))
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.matchdetailsbutton:
			try {
				Ion.with(getActivity(), helper.getMatchDetailsUri(42))
						.asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {

							@Override
							public void onCompleted(Exception e, JsonObject json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e) {
				Log.e("TestFragment", "Test failed", e);
			}
			break;
		case R.id.queuestatsbutton:
			try {
				Ion.with(getActivity(),
						helper.getQueueStatsUri("mowdownjoe", 431))
						.asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {

							@Override
							public void onCompleted(Exception e, JsonObject json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e) {
				Log.e("TestFragment", "Test failed", e);
			}
			break;
		case R.id.demotestbutton:
			try {
				Ion.with(getActivity(),
						helper.getDemoDetailsUri(42))
						.asJsonObject()
						.setCallback(new FutureCallback<JsonObject>() {

							@Override
							public void onCompleted(Exception e, JsonObject json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e) {
				Log.e("TestFragment", "Test failed", e);
			}
			break;
		case R.id.teamsearchtestbutton:
			try {
				Ion.with(getActivity(), helper.searchTeamsUri("BORT"))
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.teamhistorybutton:
			try {
				Ion.with(getActivity(), helper.getTeamMatchHistoryUri(42))
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.teamplayerbutton:
			try {
				Ion.with(getActivity(), helper.getTeamPlayersUri(42))
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.teamleaderboardbutton:
			try {
				Ion.with(getActivity(), helper.getTeamLeaderboardUri())
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.teamdetailsbutton:
			try {
				Ion.with(getActivity(), helper.getTeamDetails(42))
						.asJsonArray()
						.setCallback(new FutureCallback<JsonArray>() {

							@Override
							public void onCompleted(Exception e, JsonArray json) {
								if (e == null) {
									Log.d("PlayerTestResult:", json.toString());
								} else {
									Log.e("TestFragment", "Test failed", e);
								}
							}
						});
			} catch (NoSuchAlgorithmException e1) {
				Log.e("TestFragment", "Test failed", e1);
			}
			break;
		case R.id.pingbutton:
			Log.d("PingTest", Boolean.toString(helper.ping()));
			break;
		}
	}

}
